var mouseX = 0;
var mouseY = 0;

function callupdate() {
	update(); 
	setTimeout(callupdate, 10); 
}

function init() {
	window.addEventListener("mousemove", function(e) {
		mouseX=e.pageX-canvas.offsetLeft;
		mouseY=e.pageY-canvas.offsetTop;
	});

	if(typeof mousemove != "undefined")
		window.addEventListener("mousemove", mousemove);
	if(typeof mouseup != "undefined")
		window.addEventListener("mouseup", mouseup);

	callupdate();
}

function update()
{
    console.log("X:" + mouseX + "Y:" + mouseY);
}

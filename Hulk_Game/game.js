var fistX = 0, fistY = 0;
/* LOKI */  var lokiWidth = 40; var lokiHeight = 70; var deltaLokiX = 3; var deltaLokiY = 0; var lokiX = Math.random() * (800 - lokiWidth - 1); var lokiY = 0;
/* ULTRON */  var ultronWidth = 40; var ultronHeight = 70; var deltaUltronX = 3; var deltaUltronY = 0; var ultronX = Math.random() * (800 - ultronWidth - 1); var ultronY = 600 - ultronHeight;
/* RED SKULL */  var skullWidth = 40; var skullHeight = 70; var deltaSkullX = 3; var deltaSkullY = 0; var skullX = 0; var skullY = Math.random() * (600 - skullHeight - 1);
/* GREEN GOBLIN */  var goblinWidth = 40; var goblinHeight = 70; var deltaGoblinX = 3; var deltaGoblinY = 0; var goblinX = 800 - goblinWidth; var goblinY = Math.random() * (600 - goblinHeight - 1);
/* THANOS */ charger = 0;//0% при достигане на 100% се появява танос и има бос битка; сложен за убиване..; ръкавица??; след като го победи -> хълк с ръкавицата на танос??; 


function lokiReset() {
    var wayNumL = Math.random();
    if (wayNumL <= 1) {
        lokiX = Math.random() * (800 - lokiWidth - 1);
        lokiY = 1;
        deltaLokiX = 0;
        deltaLokiY = 3;
        charger++;
    } else if (wayNumL <= 0.75) {
        lokiX = Math.random() * (800 - lokiWidth - 1);
        lokiY = 600 - lokiHeight;
        deltaLokiX = 0;
        deltaLokiY = -3;
        charger++;
    } else if (wayNumL <= 0.5) {
        lokiX = 800 - lokiWidth;
        lokiY = Math.random() * (600 - lokiHeight - 1);
        deltaLokiX = -3;
        deltaLokiY = 0;
        charger++;
    } else if (wayNumL <= 0.25) {
        lokiX = 0;
        lokiY = Math.random() * (600 - lokiHeight - 1);
        deltaLokiX = 3;
        deltaLokiY = 0;
        charger++;
    }
}

function ultronReset() {
    var wayNumU = Math.random();
    if (wayNumU <= 1) {
        ultronX = Math.random() * (800 - ultronWidth - 1);
        ultronY = 1;
        deltaUltronX = 0;
        deltaUltronY = 3;
        charger++;
    } else if (wayNumU <= 0.75) {
        ultronX = Math.random() * (800 - ultronWidth - 1);
        ultronY = 600 - ultronWidth;
        deltaUltronX = 0;
        deltaUltronY = -3;
        charger++;
    } else if (wayNumU <= 0.5) {
        ultronX = 800 - ultronWidth;
        ultronY = Math.random() * (600 - ultronHeight - 1);
        deltaUltronX = -3;
        deltaUltronY = 0;
        charger++;
    } else if (wayNumU <= 0.25) {
        ultronX = 1;
        ultronY = Math.random() * (600 - ultronHeight - 1);
        deltaUltronX = 3;
        deltaUltronY = 0;
        charger++;
    }
}

function goblinReset() {
    var wayNumG = Math.random();
    if (wayNumG <= 1) {
        goblinX = Math.random() * (800 - goblinWidth - 1);
        goblinY = 1;
        deltaGoblinX = 0;
        deltaGoblinY = 3;
        charger++;
    } else if (wayNumG <= 0.75) {
        goblinX = Math.random() * (800 - goblinWidth - 1);
        goblinY = 600 - goblinHeight;
        deltaGoblinX = 0;
        deltaGoblinY = -3;
        charger++;
    } else if (wayNumG <= 0.5) {
        goblinX = 800 - goblinWidth;
        goblinY = Math.random() * (600 - goblinHeight - 1);
        deltaGoblinX = -3;
        deltaGoblinY = 0;
        charger++;
    } else if (wayNumG <= 0.25) {
        goblinX = 0;
        goblinY = Math.random() * (600 - goblinHeight - 1);
        deltaGoblinX = 3;
        deltaGoblinY = 0;
        charger++;
    }
}

function skullReset() {
    var wayNumS = Math.random();
    if (wayNumS <= 1) {
        skullX = Math.random() * (800 - skullWidth - 1);
        skullY = 1;
        deltaSkullX = 0;
        deltaSkullY = 3;
        charger++;
    }
    else if (wayNumS <= 0.75) {
        skullX = Math.random() * (800 - skullWidth - 1);
        skullY = 600 - skullHeight;
        deltaSkullX = 0;
        deltaSkullY = -3;
        charger++;
    } else if (wayNumS <= 0.5) {
        skullX = 800 - skullWidth;
        skullY = Math.random() * (600 - skullHeight - 1);
        deltaSkullX = -3;
        deltaSkullY = 0;
        charger++;
    } else if (wayNumS <= 0.25) {
        skullX = 0;
        skullY = Math.random() * (600 - skullHeight - 1);
        deltaSkullX = 3;
        deltaSkullY = 0;
        charger++;
    }
}



function update() {
    fistX = mouseX;
    fistY = mouseY;

    lokiX = lokiX + deltaLokiX;
    lokiY = lokiY + deltaLokiY;

    ultronX += deltaUltronX;
    ultronY += deltaUltronY;

    skullX += deltaSkullX;
    skullY += deltaSkullY;

    goblinX += deltaGoblinX;
    goblinY += deltaGoblinY;

    if (areColliding(fistX, fistY, 50, 50, lokiX, lokiY, lokiWidth, lokiHeight)) {
        lokiReset();
    }

    if (areColliding(fistX, fistY, 50, 50, goblinX, goblinY, goblinWidth, goblinHeight)) {
        goblinReset();
    }

    if (areColliding(fistX, fistY, 50, 50, ultronX, ultronY, ultronWidth, ultronHeight)) {
        ultronReset();
    }

    if (areColliding(fistX, fistY, 50, 50, skullX, skullY, skullWidth, skullHeight)) {
        skullReset();
    }

}

function draw() {
    context.fillStyle = "grey";
    context.fillRect(lokiX, lokiY, lokiWidth, lokiHeight);

    context.fillStyle = "white";
    context.fillRect(fistX, fistY, 50, 50);

    context.fillStyle = "black";
    context.fillRect(ultronX, ultronY, ultronWidth, ultronHeight);

    context.fillStyle = "red";
    context.fillRect(skullX, skullY, skullWidth, skullHeight);

    context.fillStyle = "green";
    context.fillRect(goblinX, goblinY, goblinWidth, goblinHeight);
}

function keyup(key) {
    // Show the pressed keycode in the console
    console.log("Pressed", key);
}
function mouseup() {
}
